from django.db import models

# Create your models here.

'''
检测仪器表
'''


class Monitor(models.Model):
    monitor_id = models.CharField(max_length=1000)
    add_time = models.DateTimeField(auto_now=True)
    monitor_value = models.FloatField(default=0)


'''
控制数据表
'''


class Control(models.Model):
    control_id = models.CharField(max_length=1000)
    add_time = models.DateTimeField(auto_now=True)
    write_value = models.FloatField(default=0)
    # is_used = models.BooleanField(choices=((1, '被采用'), (0, '未采用'),), default=0)


'''
是否被采用
'''


class If_Used(models.Model):
    used_id = models.CharField(max_length=1000)
    add_time = models.DateTimeField(auto_now_add=True)
    is_used = models.BooleanField(choices=((1, '被采用'), (0, '未采用'),), default=0)
