from django.contrib import admin

# Register your models here.
from . import models

# admin.site.register(models.Monitor)

# admin.site.register(models.Control)

# admin.site.register(models.If_Used)


@admin.register(models.Monitor)
class MonitorAdmin(admin.ModelAdmin):
    list_display = ('id','monitor_id', 'monitor_value','add_time')


@admin.register(models.Control)
class ControlAdmin(admin.ModelAdmin):
    list_display = ('control_id', 'write_value','add_time')


@admin.register(models.If_Used)
class UserdAdmin(admin.ModelAdmin):
    list_display = ('used_id', 'is_used','add_time')
