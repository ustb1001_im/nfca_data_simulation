from django.urls import path
from . import views

urlpatterns=[
    path('',views.index),
    path('read/',views.read_data),
    path('foreacst/',views.forecast),
    path('output/',views.output_data),

]