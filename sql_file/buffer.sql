/*
 Navicat Premium Data Transfer

 Source Server         : root
 Source Server Type    : MySQL
 Source Server Version : 50643
 Source Host           : localhost:3306
 Source Schema         : buffer

 Target Server Type    : MySQL
 Target Server Version : 50643
 File Encoding         : 65001

 Date: 28/06/2019 14:46:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gms_control
-- ----------------------------
DROP TABLE IF EXISTS `gms_control`;
CREATE TABLE `gms_control`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `control_id` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `add_time` datetime(6) NOT NULL,
  `write_value` double NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gms_control
-- ----------------------------
INSERT INTO `gms_control` VALUES (1, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/OI-311TH01-01/MonAnalog/OI-311TH01-01/MonAnalog.PV_Out#Value', '2019-06-26 01:43:02.523731', 1);
INSERT INTO `gms_control` VALUES (2, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/OI-311TH02-01/MonAnalog/OI-311TH02-01/MonAnalog.PV_Out#Value', '2019-06-26 01:43:25.398588', 112.5424);
INSERT INTO `gms_control` VALUES (3, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/FIC-311PS05/MonAnalog/FIC-311PS05/MonAnalog.PV_Out#Value', '2019-06-26 01:43:40.147678', 45245.785);
INSERT INTO `gms_control` VALUES (4, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/FIC-311PS08/MonAnalog/FIC-311PS08/MonAnalog.PV_Out#Value', '2019-06-26 01:43:50.687766', 0);
INSERT INTO `gms_control` VALUES (5, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/FIC-311PS07/MonAnalog/FIC-311PS07/MonAnalog.PV_Out#Value', '2019-06-26 01:44:01.737128', 22.5755);
INSERT INTO `gms_control` VALUES (6, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/FIC-311PS10/MonAnalog/FIC-311PS10/MonAnalog.PV_Out#Value', '2019-06-26 01:44:29.971915', 0);
INSERT INTO `gms_control` VALUES (7, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/FIC-311TH01/MonAnalog/FIC-311TH01/MonAnalog.PV_Out#Value', '2019-06-26 01:44:39.334754', 0);
INSERT INTO `gms_control` VALUES (8, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/FIC-311TH02/MonAnalog/FIC-311TH02/MonAnalog.PV_Out#Value', '2019-06-26 01:44:51.738953', 0);
INSERT INTO `gms_control` VALUES (9, 'No ID_ Flocculant ratio', '2019-06-26 01:45:01.850954', 0);
INSERT INTO `gms_control` VALUES (10, 'No ID_ Flocculant flow01', '2019-06-26 01:45:09.015051', 22.78);
INSERT INTO `gms_control` VALUES (11, 'No ID_ Flocculant flow02', '2019-06-26 02:08:39.587533', 9);
INSERT INTO `gms_control` VALUES (12, 'No ID_ Mixer cement addition01', '2019-06-26 01:45:28.218466', 0);
INSERT INTO `gms_control` VALUES (13, 'No ID_Mixer cement addition02', '2019-06-26 01:45:38.376577', 0);
INSERT INTO `gms_control` VALUES (14, 'No ID_Ash sand ratio01', '2019-06-26 01:45:46.558788', 0);
INSERT INTO `gms_control` VALUES (15, 'No ID_Ash sand ratio02', '2019-06-26 02:08:27.326006', 999);

-- ----------------------------
-- Table structure for gms_if_used
-- ----------------------------
DROP TABLE IF EXISTS `gms_if_used`;
CREATE TABLE `gms_if_used`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `used_id` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `add_time` datetime(6) NOT NULL,
  `is_used` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 61 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for gms_monitor
-- ----------------------------
DROP TABLE IF EXISTS `gms_monitor`;
CREATE TABLE `gms_monitor`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `monitor_id` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `add_time` datetime(6) NOT NULL,
  `monitor_value` double NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 351 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
