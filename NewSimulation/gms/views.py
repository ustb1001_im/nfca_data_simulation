from django.http import HttpResponse
from django.shortcuts import render
from . import models

import random


def index(request):
    controlset = read_data()
    forecast(controlset)
    context = {'controlset': controlset}
    return render(request, 'gms/index.html', context=context)


def read_data():
    query_all = models.Control.objects.all().order_by('id')
    for i in query_all:
        print(i.control_id)
    return query_all


def forecast(read_data):
    forecastdata = []
    for i in range(0, 52):
        forecastdata.append(random.randint(0, 99))
    # output_data(forecastdata)
    print(forecastdata)
    return output_data(forecastdata)


def output_data(forecast):
    n = len(forecast)
    print(n)
    for i in range(1, n + 1):
        print()
        models.Monitor.objects.filter(id=i).update(monitor_value=forecast[i - 1])
    print("所有监测数据已更新!")
    return 0
