import pymysql
import json
import time
import datetime
import sys

# sql_monitor = "SELECT id, monitor_id, CAST(add_time AS CHAR) AS add_time, monitor_value FROM newsimulation.gms_monitor;"
sql_monitor = "SELECT CAST(id AS CHAR) AS id, monitor_id, CAST(add_time AS CHAR) AS add_time, monitor_value FROM newsimulation.gms_monitor;"

# sql_ifused = "SELECT id, used_id, CAST(add_time AS CHAR) AS add_time, is_used FROM newsimulation.gms_if_used;"
sql_ifused = "SELECT CAST(id AS CHAR) AS id, used_id, CAST(add_time AS CHAR) AS add_time, CAST(is_used AS CHAR) AS is_used FROM newsimulation.gms_if_used;"

sql_monitor_inster = "insert into buffer.gms_monitor(id, monitor_id, add_time, monitor_value) values(%s,%s, %s, %s);"

sql_ifused_inster = "insert into buffer.gms_if_used(id, used_id, add_time, is_used) values(%s,%s, %s, %s);"

sql_ifused_max = "select count(*) from buffer.gms_if_used"

sql_monitor_max = "select count(*) from buffer.gms_monitor"


time_str = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime())

Loginfo = {'USER':'root', 'PSWD':'admin', 'HOST':'202.204.62.229', 'PORT':3306}  # 定义连接MySQL的登录信息
conn=pymysql.connect(host=Loginfo['HOST'],user=Loginfo['USER'],passwd=Loginfo['PSWD'],port=Loginfo['PORT'],charset='utf8')
cur=conn.cursor()

cur.execute(sql_monitor)                        # 执行SQL查询
data_monitor = cur.fetchall()
cur.execute((sql_ifused))
data_ifused = cur.fetchall()
cur.execute(sql_monitor_max)
monitor_max = cur.fetchall()
cur.execute(sql_ifused_max)
ifused_max = cur.fetchall()
if len(data_ifused) == 0:
    print(time_str + ':' + "数据源控制采用数据不存在！")
else:
    # list_ifused = list(data_ifused)
    for data in data_ifused:
        data_list = list(data)
        data_list[0] = str(int(data_list[0]) + ifused_max[0][0])
        cur.execute(sql_ifused_inster, tuple(data_list))
    cur.connection.commit()

if len(data_monitor) ==0:
    print(time_str + ':' + "数据源监测数据不存在！")
else:
    # list_monitor = list(data_monitor)
    for data in data_monitor:
        data_list = list(data)
        data_list[0] = str(int(data_list[0]) + monitor_max[0][0])
        cur.execute(sql_monitor_inster,tuple(data_list))
    cur.connection.commit()