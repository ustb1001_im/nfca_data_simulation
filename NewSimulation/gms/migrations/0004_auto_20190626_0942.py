# Generated by Django 2.1 on 2019-06-26 01:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gms', '0003_auto_20190626_0934'),
    ]

    operations = [
        migrations.AlterField(
            model_name='control',
            name='control_id',
            field=models.CharField(max_length=1000),
        ),
        migrations.AlterField(
            model_name='if_used',
            name='used_id',
            field=models.CharField(max_length=1000),
        ),
        migrations.AlterField(
            model_name='monitor',
            name='monitor_id',
            field=models.CharField(max_length=1000),
        ),
    ]
