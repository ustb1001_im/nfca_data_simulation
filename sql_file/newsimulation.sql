/*
 Navicat Premium Data Transfer

 Source Server         : root
 Source Server Type    : MySQL
 Source Server Version : 50643
 Source Host           : localhost:3306
 Source Schema         : newsimulation

 Target Server Type    : MySQL
 Target Server Version : 50643
 File Encoding         : 65001

 Date: 28/06/2019 15:25:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for auth_group
-- ----------------------------
DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE `auth_group`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for auth_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS `auth_group_permissions`;
CREATE TABLE `auth_group_permissions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auth_group_permissions_group_id_permission_id_0cd325b0_uniq`(`group_id`, `permission_id`) USING BTREE,
  INDEX `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm`(`permission_id`) USING BTREE,
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE `auth_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auth_permission_content_type_id_codename_01ab375a_uniq`(`content_type_id`, `codename`) USING BTREE,
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for auth_user
-- ----------------------------
DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE `auth_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `last_login` datetime(6) NULL DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `first_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `last_name` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for auth_user_groups
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_groups`;
CREATE TABLE `auth_user_groups`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auth_user_groups_user_id_group_id_94350c0c_uniq`(`user_id`, `group_id`) USING BTREE,
  INDEX `auth_user_groups_group_id_97559544_fk_auth_group_id`(`group_id`) USING BTREE,
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for auth_user_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_user_permissions`;
CREATE TABLE `auth_user_user_permissions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq`(`user_id`, `permission_id`) USING BTREE,
  INDEX `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm`(`permission_id`) USING BTREE,
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for django_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `django_admin_log`;
CREATE TABLE `django_admin_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `object_repr` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `content_type_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `django_admin_log_content_type_id_c4bce8eb_fk_django_co`(`content_type_id`) USING BTREE,
  INDEX `django_admin_log_user_id_c564eba6_fk_auth_user_id`(`user_id`) USING BTREE,
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for django_content_type
-- ----------------------------
DROP TABLE IF EXISTS `django_content_type`;
CREATE TABLE `django_content_type`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `model` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `django_content_type_app_label_model_76bd3d3b_uniq`(`app_label`, `model`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for django_migrations
-- ----------------------------
DROP TABLE IF EXISTS `django_migrations`;
CREATE TABLE `django_migrations`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of django_migrations
-- ----------------------------
INSERT INTO `django_migrations` VALUES (1, 'contenttypes', '0001_initial', '2019-06-25 13:08:36.199420');
INSERT INTO `django_migrations` VALUES (2, 'auth', '0001_initial', '2019-06-25 13:08:36.905197');
INSERT INTO `django_migrations` VALUES (3, 'admin', '0001_initial', '2019-06-25 13:08:37.029508');
INSERT INTO `django_migrations` VALUES (4, 'admin', '0002_logentry_remove_auto_add', '2019-06-25 13:08:37.040443');
INSERT INTO `django_migrations` VALUES (5, 'admin', '0003_logentry_add_action_flag_choices', '2019-06-25 13:08:37.056704');
INSERT INTO `django_migrations` VALUES (6, 'contenttypes', '0002_remove_content_type_name', '2019-06-25 13:08:37.145505');
INSERT INTO `django_migrations` VALUES (7, 'auth', '0002_alter_permission_name_max_length', '2019-06-25 13:08:37.173057');
INSERT INTO `django_migrations` VALUES (8, 'auth', '0003_alter_user_email_max_length', '2019-06-25 13:08:37.208324');
INSERT INTO `django_migrations` VALUES (9, 'auth', '0004_alter_user_username_opts', '2019-06-25 13:08:37.223576');
INSERT INTO `django_migrations` VALUES (10, 'auth', '0005_alter_user_last_login_null', '2019-06-25 13:08:37.280576');
INSERT INTO `django_migrations` VALUES (11, 'auth', '0006_require_contenttypes_0002', '2019-06-25 13:08:37.285209');
INSERT INTO `django_migrations` VALUES (12, 'auth', '0007_alter_validators_add_error_messages', '2019-06-25 13:08:37.304698');
INSERT INTO `django_migrations` VALUES (13, 'auth', '0008_alter_user_username_max_length', '2019-06-25 13:08:37.347545');
INSERT INTO `django_migrations` VALUES (14, 'auth', '0009_alter_user_last_name_max_length', '2019-06-25 13:08:37.380718');
INSERT INTO `django_migrations` VALUES (15, 'gms', '0001_initial', '2019-06-25 13:08:37.457336');
INSERT INTO `django_migrations` VALUES (16, 'sessions', '0001_initial', '2019-06-25 13:08:37.520700');
INSERT INTO `django_migrations` VALUES (17, 'gms', '0002_auto_20190625_2130', '2019-06-25 13:31:03.700283');
INSERT INTO `django_migrations` VALUES (18, 'gms', '0003_auto_20190626_0934', '2019-06-26 01:34:43.299726');
INSERT INTO `django_migrations` VALUES (19, 'gms', '0004_auto_20190626_0942', '2019-06-26 01:42:22.977333');

-- ----------------------------
-- Table structure for gms_control
-- ----------------------------
DROP TABLE IF EXISTS `gms_control`;
CREATE TABLE `gms_control`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `control_id` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `add_time` datetime(6) NOT NULL,
  `write_value` double NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gms_control
-- ----------------------------
INSERT INTO `gms_control` VALUES (1, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/OI-311TH01-01/MonAnalog/OI-311TH01-01/MonAnalog.PV_Out#Value', '2019-06-26 01:43:02.523731', 0);
INSERT INTO `gms_control` VALUES (2, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/OI-311TH02-01/MonAnalog/OI-311TH02-01/MonAnalog.PV_Out#Value', '2019-06-26 01:43:25.398588', 0);
INSERT INTO `gms_control` VALUES (3, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/FIC-311PS05/MonAnalog/FIC-311PS05/MonAnalog.PV_Out#Value', '2019-06-26 01:43:40.147678', 0);
INSERT INTO `gms_control` VALUES (4, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/FIC-311PS08/MonAnalog/FIC-311PS08/MonAnalog.PV_Out#Value', '2019-06-26 01:43:50.687766', 0);
INSERT INTO `gms_control` VALUES (5, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/FIC-311PS07/MonAnalog/FIC-311PS07/MonAnalog.PV_Out#Value', '2019-06-26 01:44:01.737128', 0);
INSERT INTO `gms_control` VALUES (6, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/FIC-311PS10/MonAnalog/FIC-311PS10/MonAnalog.PV_Out#Value', '2019-06-26 01:44:29.971915', 0);
INSERT INTO `gms_control` VALUES (7, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/FIC-311TH01/MonAnalog/FIC-311TH01/MonAnalog.PV_Out#Value', '2019-06-26 01:44:39.334754', 0);
INSERT INTO `gms_control` VALUES (8, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/FIC-311TH02/MonAnalog/FIC-311TH02/MonAnalog.PV_Out#Value', '2019-06-26 01:44:51.738953', 0);
INSERT INTO `gms_control` VALUES (9, 'No ID_Flocculant ratio', '2019-06-26 01:45:01.850954', 0);
INSERT INTO `gms_control` VALUES (10, 'No ID_Flocculant flow01', '2019-06-26 01:45:09.015051', 0);
INSERT INTO `gms_control` VALUES (11, 'No ID_Flocculant flow02', '2019-06-26 02:08:39.587533', 9);
INSERT INTO `gms_control` VALUES (12, 'No ID_Mixer cement addition01', '2019-06-26 01:45:28.218466', 0);
INSERT INTO `gms_control` VALUES (13, 'No ID_Mixer cement addition02', '2019-06-26 01:45:38.376577', 0);
INSERT INTO `gms_control` VALUES (14, 'No ID_Ash sand ratio01', '2019-06-26 01:45:46.558788', 0);
INSERT INTO `gms_control` VALUES (15, 'No ID_Ash sand ratio02', '2019-06-26 02:08:27.326006', 999);

-- ----------------------------
-- Table structure for gms_if_used
-- ----------------------------
DROP TABLE IF EXISTS `gms_if_used`;
CREATE TABLE `gms_if_used`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `used_id` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `add_time` datetime(6) NOT NULL,
  `is_used` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gms_if_used
-- ----------------------------
INSERT INTO `gms_if_used` VALUES (1, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/OI-311TH01-01/MonAnalog/OI-311TH01-01/MonAnalog.PV_Out#Value_state', '2019-06-26 01:47:34.774390', 0);
INSERT INTO `gms_if_used` VALUES (2, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/OI-311TH02-01/MonAnalog/OI-311TH02-01/MonAnalog.PV_Out#Value_state', '2019-06-26 01:47:44.496529', 0);
INSERT INTO `gms_if_used` VALUES (3, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/FIC-311PS05/MonAnalog/FIC-311PS05/MonAnalog.PV_Out#Value_state', '2019-06-26 01:47:53.549373', 0);
INSERT INTO `gms_if_used` VALUES (4, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/FIC-311PS08/MonAnalog/FIC-311PS08/MonAnalog.PV_Out#Value_state', '2019-06-26 01:48:04.376077', 0);
INSERT INTO `gms_if_used` VALUES (5, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/FIC-311PS07/MonAnalog/FIC-311PS07/MonAnalog.PV_Out#Value_state', '2019-06-26 01:48:12.816700', 0);
INSERT INTO `gms_if_used` VALUES (6, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/FIC-311PS10/MonAnalog/FIC-311PS10/MonAnalog.PV_Out#Value_state', '2019-06-26 01:48:19.388076', 0);
INSERT INTO `gms_if_used` VALUES (7, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/FIC-311TH01/MonAnalog/FIC-311TH01/MonAnalog.PV_Out#Value_state', '2019-06-26 01:48:27.560174', 0);
INSERT INTO `gms_if_used` VALUES (8, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/FIC-311TH02/MonAnalog/FIC-311TH02/MonAnalog.PV_Out#Value_state', '2019-06-26 01:48:35.440722', 0);
INSERT INTO `gms_if_used` VALUES (9, 'No ID_Flocculant ratio_state', '2019-06-26 01:48:41.561448', 0);
INSERT INTO `gms_if_used` VALUES (10, 'No ID_Flocculant flow01_state', '2019-06-26 01:48:47.703768', 0);
INSERT INTO `gms_if_used` VALUES (11, 'No ID_Flocculant flow02_state', '2019-06-26 01:48:55.292398', 0);
INSERT INTO `gms_if_used` VALUES (12, 'No ID_Mixer cement addition01_state', '2019-06-26 01:49:04.242328', 0);
INSERT INTO `gms_if_used` VALUES (13, 'No ID_Mixer cement addition02_state', '2019-06-26 01:49:13.946905', 0);
INSERT INTO `gms_if_used` VALUES (14, 'No ID_Ash sand ratio01_state', '2019-06-26 01:49:21.657079', 0);
INSERT INTO `gms_if_used` VALUES (15, 'No ID_Ash sand ratio02_state', '2019-06-26 01:49:28.899170', 0);

-- ----------------------------
-- Table structure for gms_monitor
-- ----------------------------
DROP TABLE IF EXISTS `gms_monitor`;
CREATE TABLE `gms_monitor`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `monitor_id` varchar(1000) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `add_time` datetime(6) NOT NULL,
  `monitor_value` double NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 53 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of gms_monitor
-- ----------------------------
INSERT INTO `gms_monitor` VALUES (1, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/FI-311TH01A/MonAnalog/FI-311TH01A/MonAnalog.PV_Out#Value', '2019-06-26 01:50:59.150422', 35);
INSERT INTO `gms_monitor` VALUES (2, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/FI-311TH01A/MonAnalog/FI-311TH01A/MonAnalog.PV_Out#Value', '2019-06-26 01:51:10.672642', 70);
INSERT INTO `gms_monitor` VALUES (3, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/DI-311TH01A/MonAnalog/DI-311TH01A/MonAnalog.PV_Out#Value', '2019-06-26 01:51:23.402083', 57);
INSERT INTO `gms_monitor` VALUES (4, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/DI-311TH02A/MonAnalog/DI-311TH02A/MonAnalog.PV_Out#Value', '2019-06-26 01:51:31.764261', 26);
INSERT INTO `gms_monitor` VALUES (5, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/DIC-311TH01B/MonAnalog/DIC-311TH01B/MonAnalog.PV_Out#Value', '2019-06-26 01:51:46.574448', 82);
INSERT INTO `gms_monitor` VALUES (6, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/DIC-311TH02B/MonAnalog/DIC-311TH02B/MonAnalog.PV_Out#Value', '2019-06-26 01:51:53.230370', 6);
INSERT INTO `gms_monitor` VALUES (7, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIS-311TH01-01/MonAnalog/HIS-311TH01-01/MonAnalog.PV_Out#Value', '2019-06-26 01:52:07.501571', 0);
INSERT INTO `gms_monitor` VALUES (8, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIS-311TH01-02/MonAnalog/HIS-311TH01-02/MonAnalog.PV_Out#Value', '2019-06-26 01:52:19.136888', 54);
INSERT INTO `gms_monitor` VALUES (9, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIS-311TH01-03/MonAnalog/HIS-311TH01-03/MonAnalog.PV_Out#Value', '2019-06-26 01:52:25.606355', 85);
INSERT INTO `gms_monitor` VALUES (10, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIS-311TH01-04/MonAnalog/HIS-311TH01-04/MonAnalog.PV_Out#Value', '2019-06-26 01:52:31.174405', 18);
INSERT INTO `gms_monitor` VALUES (11, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIS-311TH02-01/MonAnalog/HIS-311TH01-01/MonAnalog.PV_Out#Value', '2019-06-26 01:52:36.567371', 17);
INSERT INTO `gms_monitor` VALUES (12, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIS-311TH02-02/MonAnalog/HIS-311TH01-02/MonAnalog.PV_Out#Value', '2019-06-26 01:52:44.550046', 87);
INSERT INTO `gms_monitor` VALUES (13, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIS-311TH02-03/MonAnalog/HIS-311TH01-03/MonAnalog.PV_Out#Value', '2019-06-26 01:52:54.941831', 2);
INSERT INTO `gms_monitor` VALUES (14, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIS-311TH02-04/MonAnalog/HIS-311TH01-04/MonAnalog.PV_Out#Value', '2019-06-26 01:53:02.721708', 14);
INSERT INTO `gms_monitor` VALUES (15, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/PI-311TH01/MonAnalog/PI-311TH01/MonAnalog.PV_Out#Value', '2019-06-26 01:53:09.213219', 93);
INSERT INTO `gms_monitor` VALUES (16, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/PI-311TH02/MonAnalog/PI-311TH02/MonAnalog.PV_Out#Value', '2019-06-26 01:53:17.601176', 5);
INSERT INTO `gms_monitor` VALUES (17, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIC-311PS01A/Valve/HIS-311PS01A/Valve.FbkOpenOut#Value', '2019-06-26 01:53:25.057507', 27);
INSERT INTO `gms_monitor` VALUES (18, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIC-311PS01B/Valve/HIS-311PS01B/Valve.FbkOpenOut#Value', '2019-06-26 01:53:37.231520', 24);
INSERT INTO `gms_monitor` VALUES (19, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIC-311PS01C/Valve/HIS-311PS01C/Valve.FbkOpenOut#Value', '2019-06-26 01:53:44.203222', 64);
INSERT INTO `gms_monitor` VALUES (20, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIC-311PS01D/Valve/HIS-311PS01D/Valve.FbkOpenOut#Value', '2019-06-26 01:53:51.099677', 81);
INSERT INTO `gms_monitor` VALUES (21, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIC-311PS02A/Valve/HIS-311PS02A/Valve.FbkOpenOut#Value', '2019-06-26 01:54:02.938855', 73);
INSERT INTO `gms_monitor` VALUES (22, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIC-311PS02B/Valve/HIS-311PS02B/Valve.FbkOpenOut#Value', '2019-06-26 01:54:11.805919', 79);
INSERT INTO `gms_monitor` VALUES (23, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIC-311PS02C/Valve/HIS-311PS02C/Valve.FbkOpenOut#Value', '2019-06-26 01:54:19.879064', 53);
INSERT INTO `gms_monitor` VALUES (24, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIC-311PS02D/Valve/HIS-311PS02D/Valve.FbkOpenOut#Value', '2019-06-26 01:54:27.172500', 33);
INSERT INTO `gms_monitor` VALUES (25, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIC-311TH01B/Valve/HIS-311TH01B/Valve.FbkOpenOut#Value', '2019-06-26 01:54:33.446049', 56);
INSERT INTO `gms_monitor` VALUES (26, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIC-311TH01C/Valve/HIS-311TH01C/Valve.FbkOpenOut#Value', '2019-06-26 01:54:39.475781', 89);
INSERT INTO `gms_monitor` VALUES (27, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIC-311TH01D/Valve/HIS-311TH01D/Valve.FbkOpenOut#Value', '2019-06-26 01:54:46.024157', 60);
INSERT INTO `gms_monitor` VALUES (28, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIC-311TH02B/Valve/HIS-311TH02B/Valve.FbkOpenOut#Value', '2019-06-26 01:54:52.648853', 6);
INSERT INTO `gms_monitor` VALUES (29, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIC-311TH02C/Valve/HIS-311TH02C/Valve.FbkOpenOut#Value', '2019-06-26 01:55:00.947178', 55);
INSERT INTO `gms_monitor` VALUES (30, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIC-311TH02D/Valve/HIS-311TH02D/Valve.FbkOpenOut#Value', '2019-06-26 01:55:10.593379', 47);
INSERT INTO `gms_monitor` VALUES (31, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/LI-311TH01/MonAnalog/LI-311TH01/MonAnalog.PV_Out#Value', '2019-06-26 01:55:17.543310', 53);
INSERT INTO `gms_monitor` VALUES (32, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/LI-311TH02/MonAnalog/LI-311TH02/MonAnalog.PV_Out#Value', '2019-06-26 01:55:27.296462', 99);
INSERT INTO `gms_monitor` VALUES (33, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/LI-210BN01/MonAnalog/LI-210BN01/MonAnalog.PV_Out#Value', '2019-06-26 01:55:38.933688', 45);
INSERT INTO `gms_monitor` VALUES (34, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/LI-210BN02/MonAnalog/LI-210BN02/MonAnalog.PV_Out#Value', '2019-06-26 01:55:44.730706', 58);
INSERT INTO `gms_monitor` VALUES (35, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/LI-210AG01A/MonAnalog/LI-210AG01A/MonAnalog.PV_Out#Value', '2019-06-26 01:55:51.472209', 69);
INSERT INTO `gms_monitor` VALUES (36, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/LI-210AG02A/MonAnalog/LI-210BAG02A/MonAnalog.PV_Out#Value', '2019-06-26 01:56:04.301088', 51);
INSERT INTO `gms_monitor` VALUES (37, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/LI-210AG01B/MonAnalog/LI-210AG01B/MonAnalog.PV_Out#Value', '2019-06-26 01:56:14.664117', 64);
INSERT INTO `gms_monitor` VALUES (38, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/LI-210AG02B/MonAnalog/LI-210AG02B/MonAnalog.PV_Out#Value', '2019-06-26 01:56:28.780871', 9);
INSERT INTO `gms_monitor` VALUES (39, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIS-210AG01-01/MonAnalog/HIS-210AG01-01/MonAnalog.PV_Out#Value', '2019-06-26 01:56:35.653498', 86);
INSERT INTO `gms_monitor` VALUES (40, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIS-210AG01-02/MonAnalog/HIS-210AG01-02/MonAnalog.PV_Out#Value', '2019-06-26 01:56:42.350680', 32);
INSERT INTO `gms_monitor` VALUES (41, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIS-210AG02-01/MonAnalog/HIS-210AG02-01/MonAnalog.PV_Out#Value', '2019-06-26 01:56:48.841877', 65);
INSERT INTO `gms_monitor` VALUES (42, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/HIS-210AG02-02/MonAnalog/HIS-210AG02-02/MonAnalog.PV_Out#Value', '2019-06-26 01:56:56.939833', 35);
INSERT INTO `gms_monitor` VALUES (43, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/WI-210FDM01/MonAnalog/WI-210FDM01/MonAnalog.PV_Out#Value', '2019-06-26 01:57:08.246004', 27);
INSERT INTO `gms_monitor` VALUES (44, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/WI-210FDM02/MonAnalog/WI-210FDM02/MonAnalog.PV_Out#Value', '2019-06-26 01:57:15.759410', 81);
INSERT INTO `gms_monitor` VALUES (45, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/WQI-210FDM01/MonAnalog/WQI-210FDM01/MonAnalog.PV_Out#Value', '2019-06-26 01:57:22.717434', 97);
INSERT INTO `gms_monitor` VALUES (46, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/WQI-210FDM02/MonAnalog/WQI-210FDM02/MonAnalog.PV_Out#Value', '2019-06-26 01:57:30.058853', 15);
INSERT INTO `gms_monitor` VALUES (47, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/FI-210AG01/MonAnalog/FI-210AG01/MonAnalog.PV_Out#Value', '2019-06-26 01:57:36.110961', 89);
INSERT INTO `gms_monitor` VALUES (48, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/FI-210AG02/MonAnalog/FI-210AG02/MonAnalog.PV_Out#Value', '2019-06-26 01:57:44.893747', 61);
INSERT INTO `gms_monitor` VALUES (49, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/DI-210AG01/MonAnalog/DI-210AG01/MonAnalog.PV_Out#Value', '2019-06-26 01:57:52.603119', 91);
INSERT INTO `gms_monitor` VALUES (50, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/DI-210AG02/MonAnalog/DI-210AG02/MonAnalog.PV_Out#Value', '2019-06-26 01:58:03.112196', 93);
INSERT INTO `gms_monitor` VALUES (51, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/AI-311TH01/MonAnalog/AI-311TH01/MonAnalog.PV_Out#Value', '2019-06-27 12:19:42.051355', 74);
INSERT INTO `gms_monitor` VALUES (52, 'SOUTHEASTORE_PRJ_OS::/List of all structure instances/AI-311TH02/MonAnalog/AI-311TH02/MonAnalog.PV_Out#Value', '2019-06-27 12:19:50.636283', 52);

SET FOREIGN_KEY_CHECKS = 1;
